# Symfony products

## Installation

**Requirements**

- PHP 8.1
- Mysql/MariaDB
- Composer https://getcomposer.org/download/
- Symfony (for local env) https://symfony.com/download

## Setup (with docker)

- Install dependencies
```
composer install
```
- Start docker container
```
docker compose up -d
```
- Create DB
```
symfony console doctrine:database:create --if-not-exists
symfony console doctrine:migration:migrate
```
- Loading data fixtures
```
symfony console doctrine:fixtures:load
```
- Start the web server
```
symfony serve
```

## Setup (without docker)

- Install dependencies
```
composer install
```
- Update the `DATABASE_URL` environment variable in `.env` or `.env.local`
- Create DB
```
bin/console doctrine:database:create --if-not-exists
bin/console doctrine:migration:migrate
```
- Loading data fixtures
```
bin/console doctrine:fixtures:load
```
- Start the web server
```
symfony serve
```
or
```
php -S localhost:8000 -t public/
```
