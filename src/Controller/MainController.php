<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function index(Request $request, ProductRepository $repository, PaginatorInterface $paginator): Response
    {
        $sortField = $request->query->get('sort');
        if (null === $sortField) {
            $request->query->set('sort', 'p.createdAt');
            $request->query->set('direction', 'desc');
        }

        $pagination = $paginator->paginate(
            $repository->getQueryBuilder(),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('main/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/information', name: 'app_information')]
    public function information(ProductRepository $repository): Response
    {
        $codeTypes = [];
        foreach ($repository->getGroupedCodes() as $code) {
            $code['types'] = [];
            $codeTypes[$code['code']] = $code;
        }
        foreach ($repository->getGroupedCodeTypes() as $codeType) {
            $codeTypes[$codeType['code']]['types'][] = $codeType;
        }

        return $this->render('main/information.html.twig', [
            'codeTypes' => $codeTypes,
        ]);
    }
}
