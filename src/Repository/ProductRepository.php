<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('p');
    }

    public function getGroupedCodes(): array
    {
        return $this->createQueryBuilder('p')
            ->select('p.code')
            ->addSelect('COUNT(p.id) AS countPerCode')
            ->addSelect('SUM(p.price) AS pricePerCode')
            ->addOrderBy('pricePerCode', 'desc')
            ->addGroupBy('p.code')
            ->getQuery()
            ->getResult();
    }

    public function getGroupedCodeTypes(): array
    {
        return $this->createQueryBuilder('p')
            ->select('p.code')
            ->addSelect('p.type')
            ->addSelect('COUNT(p.id) AS countPerCodeType')
            ->addSelect('SUM(p.price) AS pricePerCodeType')
            ->addOrderBy('p.type')
            ->addGroupBy('p.code')
            ->addGroupBy('p.type')
            ->getQuery()
            ->getResult();
    }
}
